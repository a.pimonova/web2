function cost() {
let regexpPrice = /^(([1-9]\d*)|0)((\.|,)\d{1,2})?$/;
let regexpQuantity = /^[1-9]\d*$|^0$/m;
let p = document.getElementsByName("price");
let q = document.getElementsByName("q");
p = p[0].value;
q = q[0].value;
let isDataCorrect = regexpPrice.test(p) && regexpQuantity.test(q);
if(!isDataCorrect) {
alert("Неверный формат входных данных.");
return false;
}
if(p.search(",") !== -1) {
p = p.replace(",", ".");
}
let res = document.getElementById("result");
p = parseFloat(p);
q = parseInt(q);
res.innerHTML = p*q;
}
window.addEventListener("DOMContentLoaded", function (event) {
console.log("DOM fully loaded and parsed");
let b = document.getElementById("button");
b.addEventListener("click", cost);
});
